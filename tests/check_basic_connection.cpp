/*
 * This file is part of switcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#undef NDEBUG  // get assert in release mode

#include <atomic>
#include <cassert>
#include <condition_variable>

#include "switcher/infotree/information-tree.hpp"
#include "switcher/infotree/json-serializer.hpp"
#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

static bool success = false;
static std::atomic<bool> do_continue{true};
static std::condition_variable cond_var{};
static std::mutex mut{};

void wait_until_success() {
  // wait 3 seconds
  unsigned int count = 30;
  while (do_continue) {
    std::unique_lock<std::mutex> lock(mut);
    if (count == 0) {
      do_continue = false;
    } else {
      --count;
      cond_var.wait_for(lock, std::chrono::seconds(1),
                        []() { return !do_continue.load(); });
    }
  }
}

void notify_success() {
  std::unique_lock<std::mutex> lock(mut);
  success = true;
  do_continue = false;
  cond_var.notify_one();
}

int main() {
  using namespace switcher;
  using namespace switcher::quiddity::property;
  using namespace switcher::quiddity::claw;

  Switcher::ptr swit = Switcher::make_switcher("test_manager");

  // gets a dummysink quid that accepts anything.
  auto reader = swit->quids<MPtr(&quiddity::Container::create)>(
                        "dummysink", "reader", nullptr)
                    .get();
  assert(reader);
  // and a videotestsrc to be sure we have some actual data passing through
  auto writer = swit->quids<MPtr(&quiddity::Container::create)>(
                        "videotestsrc", "writer", nullptr)
                    .get();
  assert(writer);
  // starts the video.
  writer->prop<MPtr(&PBag::set_str_str)>("started", "true");

  // this registers a callback to the frame-received property of the dummysink.
  // this will call notify_success once the sink receives a frame.
  assert(0 !=
         reader->prop<MPtr(&PBag::subscribe)>(
             reader->prop<MPtr(&PBag::get_id)>("frame-received"), [&]() {
               if (reader->prop<MPtr(&PBag::get<bool>)>(
                       reader->prop<MPtr(&PBag::get_id)>("frame-received"))) {
                 notify_success();
               }
             }));
  // connects the quiddities
  auto res_id = reader->claw<MPtr(&Claw::try_connect)>(writer->get_id());
  // We need to wait here because the sfid of the Follower shmdata is not set
  // until the on_server_connected callback is called and it is only called when
  // some data is received.
  wait_until_success();

  // gets the swid associated with the video label and then gets the shmpath
  // associated with the swid
  auto writer_swid = writer->claw<MPtr(&Claw::get_swid)>("video");
  auto writer_shmpath =
      writer->claw<MPtr(&Claw::get_writer_shmpath)>(writer_swid);

  // checks to see if the sfid property exists in the infotree of the follower
  // shmdata
  sfid_t sfid_from_shmdata_tree =
      reader->tree<MPtr(&InfoTree::get_copy)>()->branch_get_value(
          ".shmdata.reader." + writer_shmpath + ".sfid");
  auto sfid_from_reader_claw =
      reader->claw<MPtr(&Claw::get_follower_sfid)>(writer_shmpath);
  assert(sfid_from_shmdata_tree == sfid_from_reader_claw);
  // and then disconnect
  assert(reader->claw<MPtr(&Claw::disconnect)>(res_id));

  return 0;
}
